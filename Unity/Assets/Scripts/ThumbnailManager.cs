﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using System;
public class ThumbnailManager : MonoBehaviour
{

	public Transform	container;
	public GameObject	prefab;

	private const int m_Spacer = 22;//space between thumbnails
	private const int m_MaxPerRow = 5;//max thumbnails per row
	private const int m_MaxThumbnailsDisplayed = 20;//Max thumbnails on screen
	private const int m_PaddingFromLeft = 100;//Thumbnail padding from left
	private const int m_ThumbWidthAndHeight = 172;
	private const int m_MaxRows = 4;

	private int		m_ThumbInRowCount = 0;
	private Vector2 m_LastPositionOfThumbnail = new Vector2(100, -100);
	private bool	m_LockedMin = false, m_LockedMax = false;
	private int		m_CountOnScreen = 0;

	private List<ThumbnailVO>	m_thumbnailVOList = new List<ThumbnailVO>();
	public Thumbnail[]			m_ThumbnailArray;
	private bool m_ShiftDown = false;
	private bool m_ShiftUp = false;
	void Start()
	{
		createThumbnailVOList();
		createThumbnailPrefabs();
	}

	private void createThumbnailVOList()
	{
		ThumbnailVO thumbnailVO;
		//Loads correct sprite from one of 4 sprite map
		string _SpriteName = "";
		//As each spritemap contains 250 sprites, and unity generated the names when splitting we use the number in the name. E.g(1_249)
		int _Counter = 249;
		for (int i = 0; i < 1000; i++)
		{
			switch (i)
			{
				//Spritemap 1 -contains sprites 0 - 249
				case int n when n <= 249:
					_SpriteName = "1_" + _Counter.ToString();
					_Counter--;
					break;
				//Spritemap 2 -contains sprites 250 - 499
				case int n when n >= 250 && n <= 499:
					if (n == 250)
						_Counter = 249;
					_SpriteName = "2_" + _Counter.ToString();
					_Counter--;
					break;
				//Spritemap 3 -contains sprites 500 - 749
				case int n when n >= 500 && n <= 749:
					if (n == 500)
						_Counter = 249;
					_SpriteName = "3_" + _Counter.ToString();
					_Counter--;
					break;
				//Spritemap 4 -contains sprites  750 - 999
				case int n when n >= 750 && n <= 999:
					if (n == 750)
						_Counter = 249;
					_SpriteName = "4_" + _Counter.ToString();
					_Counter--;
					break;
			}
			thumbnailVO = new ThumbnailVO(i.ToString(), _SpriteName);
			m_thumbnailVOList.Add(thumbnailVO);
		}
	}

	private void createThumbnailPrefabs()
	{
		GameObject gameObj;
		m_ThumbnailArray = new Thumbnail[m_MaxThumbnailsDisplayed];
		for (int i = 0; i < m_MaxThumbnailsDisplayed; i++)
		{
			gameObj = Instantiate(prefab);
			gameObj.transform.SetParent(container, false);
			gameObj.GetComponent<Thumbnail>().thumbnailVO = m_thumbnailVOList[i];
			Debug.Log(m_ThumbnailArray.Length.ToString());
			m_ThumbnailArray[i] = gameObj.GetComponent<Thumbnail>();
			//Gerate thumbnails pos in grid
			Gridify(i);
		}
		//Set current count of thumbnails on screen.
		m_CountOnScreen = m_MaxThumbnailsDisplayed;

	}

	private void Update()
	{
		if (Input.GetAxis("Mouse ScrollWheel") != 0)
		{
			if (Input.mouseScrollDelta.y == 1)
			{
				ShiftGridDown();
				 m_ShiftUp= !m_ShiftUp;

			}
			else if (Input.mouseScrollDelta.y == -1)
			{
				ShiftGridUp();
				m_ShiftDown = !m_ShiftDown;
			}
		}
	}
	//Can miss the scroll input- not desireble 
	/*private void FixedUpdate()
	{
		if (Input.GetAxis("Mouse ScrollWheel") != 0)
		{
			if (Input.mouseScrollDelta.y == 1)
			{
				ShiftGridDown();
			}
			else if (Input.mouseScrollDelta.y == -1)
			{
				ShiftGridUp();
			}
		}
	}*/

	//Generates grid using the thumbnails
	private void Gridify(int p_Index)
	{
		//sets starting pos
		m_ThumbnailArray[p_Index].transform.localPosition = new Vector3(m_LastPositionOfThumbnail.x, m_LastPositionOfThumbnail.y, 0);
		
		//update next cell position ready for next thumbnail
		m_LastPositionOfThumbnail.x += m_Spacer + m_ThumbWidthAndHeight;
		
		//update count
		m_ThumbInRowCount++;
		
		//max thumbnails in row reached, drop down to next row
		if (m_ThumbInRowCount >= m_MaxPerRow)
		{
			//set left padding
			m_LastPositionOfThumbnail.x = m_PaddingFromLeft;
			
			//set new y position to be lower
			m_LastPositionOfThumbnail.y -= m_Spacer + m_ThumbWidthAndHeight;
			
			//reset row count
			m_ThumbInRowCount = 0;
		}
	}
	//remove first 5 elemeents. add 5 more to end
	private void ShiftGridUp()
	{
		Debug.Log("ScrollDown");
		if (!m_LockedMax){
			//Loops over the last 5 elements to update their values
			for (int i = 0; i < m_MaxPerRow; i++){
				
				//Stops scrolling past 999
				if (m_CountOnScreen == 995)
					m_LockedMax = true;

				//Stops scrolling below 0
				if (m_CountOnScreen > m_MaxThumbnailsDisplayed)
					m_LockedMin = false;
				
				//Goes through array at starting pos 20 the updates the data based on the position of the data in m_thumbnailVOList
				m_ThumbnailArray[(m_ThumbnailArray.Length - m_MaxPerRow) + i].thumbnailVO = m_thumbnailVOList[m_CountOnScreen];
				
				//What data element were on
				m_CountOnScreen++;
			}
			//loops 20 times to shift all other values
			for (int i = 0; i < m_ThumbnailArray.Length - m_MaxPerRow; i++){
				//Calculates what the lowest data we can use is by taking in the current data on screen, and subtracting the max amount of rows: 3
				m_ThumbnailArray[i].thumbnailVO = m_thumbnailVOList[(m_CountOnScreen - (m_MaxPerRow * m_MaxRows) + i)];
			}

            for (int i = 0; i < m_ThumbnailArray.Length; i++)
            {
                if (m_ShiftDown)
                {
					Vector3 _Cache = m_ThumbnailArray[i].transform.localPosition;
					m_ThumbnailArray[i].transform.localPosition = new Vector3(_Cache.x, _Cache.y - (m_ThumbWidthAndHeight / 2), 0);
					m_ShiftUp = true;

				}
				else
                {
					Vector3 _Cache = m_ThumbnailArray[i].transform.localPosition;
					m_ThumbnailArray[i].transform.localPosition = new Vector3(_Cache.x, _Cache.y + (m_ThumbWidthAndHeight/2f), 0);
					m_ShiftUp = false;
				}
			}
		}
	}

	private void ShiftGridDown()
	{
		Debug.Log("ScrollUp");

		if (!m_LockedMin){
			for (int i = 0; i < m_MaxPerRow; i++){
				//Goes through array at starting pos 20 the updates the data based on the position of the data in m_thumbnailVOList
				m_ThumbnailArray[4 - i].thumbnailVO = m_thumbnailVOList[m_CountOnScreen - (m_MaxPerRow * m_MaxRows) -1];
				
				//What data element were on
				m_CountOnScreen--;
			}
			//loops 20 times to shift all other values
			for (int i = m_MaxPerRow; i < m_ThumbnailArray.Length; i++)
			{
				//Calculates what the lowest data we can use is by taking in the current data on screen, and subtracting the max amount of rows: 3
				m_ThumbnailArray[i].thumbnailVO = m_thumbnailVOList[(m_CountOnScreen - (m_MaxPerRow * m_MaxRows) + i)];
			}
			//Stops scrolling past 0
			if (m_CountOnScreen == m_MaxThumbnailsDisplayed)
				m_LockedMin = true;
			//Stops scrolling past 999
			if (m_CountOnScreen < 995)
				m_LockedMax = false;

			for (int i = 0; i < m_ThumbnailArray.Length; i++)
			{
				if (m_ShiftUp)
				{
					Vector3 _Cache = m_ThumbnailArray[i].transform.localPosition;
					m_ThumbnailArray[i].transform.localPosition = new Vector3(_Cache.x, _Cache.y + (m_ThumbWidthAndHeight/2), 0);
					m_ShiftDown = true;
				}
				else
				{
					Vector3 _Cache = m_ThumbnailArray[i].transform.localPosition;
					m_ThumbnailArray[i].transform.localPosition = new Vector3(_Cache.x, _Cache.y - m_ThumbWidthAndHeight/2, 0);
					m_ShiftDown = false;
				}
			}
		}
	}
}
